<?php
	
	
acf_add_options_page(array(
		'page_title' 	=> 'API Config',
		'capability'	=> 'activate_plugins',
		
	));
	
	
	if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5a99bab31b575',
	'title' => 'Configuration Options',
	'fields' => array(
		array(
			'key' => 'field_5a99bac31cd9a',
			'label' => 'API Key',
			'name' => 'api_key',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-api-config',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
	