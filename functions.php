<?php
	//Load ACF
	require( get_template_directory() . '/acf.php' );
	
	
	//Dashboard Edits
	function remove_menus(){
	 if(api_user_role() == 'editor' || api_user_role() == 'contributor'){
		   remove_menu_page( 'users.php' );                  //Users
		   remove_menu_page( 'tools.php' );                  //Tools
		   remove_menu_page( 'options-general.php' );        //Settings
		   remove_menu_page( 'edit.php?post_type=page' );    //Pages
		   remove_menu_page( 'edit-comments.php' );          //Comments
		   remove_menu_page( 'themes.php' );                 //Appearance
		   remove_menu_page( 'plugins.php' );                //Plugins
		    remove_menu_page( 'jetpack' );                    //Jetpack* 
	 }
	
	}
	add_action( 'admin_menu', 'remove_menus' );
	function api_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/sapcmslogo.png);
		height:65px;
		width:320px;
		background-size: 312px 80px;
		background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
	<?php }
	add_action( 'login_enqueue_scripts', 'api_login_logo' );
	function api_remove_help_tabs( $old_help, $screen_id, $screen ){
	    $screen->remove_help_tabs();
	    return $old_help;
	}
	add_filter( 'contextual_help', 'api_remove_help_tabs', 999, 3 );
	// Admin footer modification
	function remove_footer_admin (){
	    echo '<span id="footer-thankyou"><a href="http://cms.vantelo.com" target="_blank">Vantelo, LLC</a></span>';
	}
	add_filter('admin_footer_text', 'remove_footer_admin');
	
	//Filter Post Results for API
	function api_results_filter( $posts ) {
		$filtered_posts = array();
		foreach ( $posts as $post ) {
			$post->tk = 'tk!';
			$filtered_posts[] = $post;
		 
		} 
		return $filtered_posts ;
	}
	add_filter( 'posts_results', 'api_results_filter' );

	//Send an Email with Contributor Posts
	function submit_send_email ($post) {
		if ( current_user_can('contributor') ) {
			$user_info = get_userdata ($post->post_author);
			$strTo = array ('azitzer@gmail.com','tk@terrifickid.net');
			$strSubject = 'Your website name: ' . $user_info->user_nicename . ' submitted a post';
			$strMessage = 'A post "' . $post->post_title . '" by ' . $user_info->user_nicename . ' was submitted for review at ' . wp_get_shortlink ($post->ID) . '&preview=true. Please proof.';
			wp_mail( $strTo, $strSubject, $strMessage );
		}
	}
	add_action( 'draft_to_pending', 'submit_send_email' );
	add_action( 'auto-draft_to_pending', 'submit_send_email' );


	//Echo API OUTPUT
	function api_reponse(){
		api_secure();
		$query = new WP_Query( $_REQUEST );
		$response['status'] = 1;
				$response['version'] = 1.1;
				$response['numpages'] = $numpages;
				$response['posts'] = $query->posts;
	print_r(json_encode($response));
	}
	
	
	//Get User Role
	function api_user_role( $user = null ) {
		$user = $user ? new WP_User( $user ) : wp_get_current_user();
		return $user->roles ? $user->roles[0] : false;
	}
	
	//Function Secure API
	function api_secure(){
	 	if($_SERVER["HTTPS"] != "on") die ('No Access.');
		if($_REQUEST['key'] != get_field('api_key', 'option')) die('No Access.');
	}